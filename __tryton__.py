# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Project Operation',
    'name_de_DE': 'Projekt Einsatzbericht',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds functionalities for operational reports to project
      management.
    ''',
    'description_de_DE': '''
    - Fügt dem Projektmanagement Funktionalität für Einsatzberichte hinzu.
    ''',
    'depends': [
        'timesheet',
        'project',
        'party_addressee',
        'party_rollup_person',
        'party_type_salutation',
    ],
    'xml': [
        'operation.xml',
        'project.xml',
        'configuration.xml',
        'party.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
