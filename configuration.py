#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelSingleton, fields
from trytond.pyson import Eval


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Project Configuration'
    _name = 'project.configuration'
    _description = __doc__

    project_sequence = fields.Property(fields.Many2One('ir.sequence',
            'Project Sequence', domain=[
            ('company', 'in', [Eval('company'), False]),
            ('code', '=', 'project.work'),
        ], required=True, depends=['company']))

    project_operation_sequence = fields.Property(fields.Many2One('ir.sequence',
            'Project Operation Sequence', domain=[
            ('company', 'in', [Eval('company'), False]),
            ('code', '=', 'project.operation'),
        ], required=True, depends=['company']))

    operation_email_subject = fields.Char('Operation Report E-Mail Subject',
        translate=True, loading='lazy')
    operation_email_message = fields.Text('Operation Report E-Mail Message',
        translate=True, loading='lazy')

Configuration()
