# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import Eval, Equal, Not, Or, Bool
from trytond.transaction import Transaction
from trytond.backend import TableHandler
from trytond.pool import Pool


class Work(ModelSQL, ModelView):
    _name = 'project.work'

    operations = fields.One2Many('project.operation', 'project_work', 'Operations',
            states={
                'invisible': Not(Equal(Eval('type'), 'project')),
                'readonly': Not(Bool(Eval('code')))
            }, depends=['type', 'code', 'party'], context={
                'project_work': Eval('active_id'),
                'party': Eval('party'),
            }, domain=[
                ('party', '=', Eval('party'))
            ])
    code = fields.Char('Code', readonly=True)
    contact_person = fields.Many2One('party.party', 'Contact Person',
            domain=[
                ('party_type', '=', 'person'),
                ('organization_members.organization', 'in', [Eval('party'),]),
            ], states={
                'invisible': Not(Equal(Eval('type'), 'project')),
                'required': Equal(Eval('type'), 'project')
            }, depends=['party', 'type'])
    site_of_operation = fields.Many2One('party.address', 'Site of Operation',
            domain=[('party', '=', Eval('party'))],
            states={
                'invisible': Not(Equal(Eval('type'), 'project')),
                'required': Equal(Eval('type'), 'project'),
            }, depends=['party', 'type'])

    def __init__(self):
        super(Work, self).__init__()
        self.party = copy.copy(self.party)
        self.party.domain = copy.copy(self.party.domain)
        self.party.context = copy.copy(self.party.context)
        self.party.states = copy.copy(self.party.states)
        self.party.domain.append(('party_type', '=', 'organization'))
        self.party.context['party_type'] = 'organization'
        if 'required' in self.party.states:
            self.party.states['required'] = Or(
                        self.party.states['required'],
                        Equal(Eval('type'), 'project'),
                    )
        else:
            self.party.states['required'] = Equal(Eval('type'), 'project')
        if 'type' not in self.party.depends:
            self.party.depends = copy.copy(self.party.depends)
            self.party.depends.append('type')

        if self.party.on_change is None:
            self.party.on_change = []
        for field in ['party', 'site_of_operation', 'contact_person']:
            if field not in self.party.on_change:
                self.party.on_change += [field]
        self._rpc.update({
            'on_change_party': False
            })
        self._reset_columns()

    def init(self, module_name):
        table = TableHandler(Transaction().cursor, self, module_name)
        if table.column_exist('contact_person'):
            table.not_null_action('contact_person', action='remove')
        if table.column_exist('ordering_date'):
            table.drop_column('ordering_date', exception=True)
        super(Work, self).init(module_name)

    def default_party(self):
        return Transaction().context.get('party', False)

    def default_contact_person(self):
        party_obj = Pool().get('party.party')
        if Transaction().context.get('party'):
            party = party_obj.browse(Transaction().context['party'])
            if len(party.person_members) == 1:
                return party.person_members[0].id
        return False

    def on_change_party(self, vals):
        party_obj = Pool().get('party.party')
        res = {
            'site_of_operation': False,
            'contact_person': False,
            }
        if vals.get('party'):
            party = party_obj.browse(vals['party'])
            res['site_of_operation'] = party_obj.address_get(party.id,
                'site_operation')
            if len(party.person_members) == 1:
                res['contact_person'] = party.person_members[0].id
        else:
            res['contract'] = False
        return res


    def create(self, values):
        sequence_obj = Pool().get('ir.sequence')
        values = values.copy()
        if not values.get('code'):
            values['code'] = sequence_obj.get('project.work')
        return super(Work, self).create(values)

    def get_rec_name(self, ids, name):
        if not ids:
            return {}
        res = {}
        for work in self.browse(ids):
            res[work.id] = '[' + work.code + '] ' + work.name
        return res

    def search_rec_name(self, name, clause):
        ids = self.search(['OR',
            ('code',) + tuple(clause[1:]),
            ('name',) + tuple(clause[1:]),
            ], order=[])
        if ids:
            return [('id', 'in', ids)]
        return [('name',) + tuple(clause[1:])]

    def create_operation(self, project):
        operation_obj = Pool().get('project.operation')
        timesheet_line_obj = Pool().get('timesheet.line')

        if isinstance(project, (int, long)):
            project = self.browse(project)

        timesheet_lines_to_process = self._get_timesheet_lines_to_process(
                project)
        if not timesheet_lines_to_process:
            return False

        vals = self._get_operation_vals_from_project(project)
        new_id = operation_obj.create(vals)

        line_ids = [x.id for x in timesheet_lines_to_process]
        with Transaction().set_user(0, set_context=True):
            timesheet_line_obj.write(line_ids, {'operation': new_id})
        return new_id

    def _get_timesheet_lines_to_process(self, project):
        res = [x for x in project.timesheet_lines if not x.operation]
        if project.children:
            for child in project.children:
                res.extend(self._get_timesheet_lines_to_process(child))
        return res

    def _get_operation_vals_from_project(self, project):
        res = {
            'party': project.party.id,
            'project_work': project.id,
            'contact_person': project.contact_person,
            'location_address': project.site_of_operation,
        }
        return res

Work()

class GenerateOperation(Wizard):
    'Generate Operation'
    _name = 'project.work.generate_operation'
    states = {
        'init': {
            'result': {
                'type': 'action',
                'action': '_generate',
                'state': 'end',
            },
        },
    }

    def _generate(self, data):
        pool = Pool()
        project_obj = pool.get('project.work')
        operation_obj = pool.get('project.operation')
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')

        projects = project_obj.browse(data.get('ids', -1))

        if projects:
            project = projects[0]
            new_id = project_obj.create_operation(project)
            if not new_id:
                return {}

            args = [('state', '=', 'draft'), ('project_work', '=', project.id)]
            operation_ids = operation_obj.search(args)

            model_data_ids = model_data_obj.search([
                ('fs_id', '=', 'act_operation_form_draft'),
                ('module', '=', 'project_operation'),
                ('inherit', '=', False),
                ], limit=1)
            model_data = model_data_obj.browse(model_data_ids[0])
            res = act_window_obj.read(model_data.db_id)
            res['domain'] = str([
                ('id', 'in', operation_ids),
                ])
            if len(operation_ids) == 1:
                res['res_id'] = operation_ids[0]
                res['views'].reverse()
        return res

GenerateOperation()

