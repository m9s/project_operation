# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Get, If


class Line(ModelSQL, ModelView):
    _name = 'timesheet.line'

    operation = fields.Many2One('project.operation', 'Operation')

    def __init__(self):
        super(Line, self).__init__()
        self.work = copy.copy(self.work)
        if self.work.domain is None:
            self.work.domain = []
        self.work.domain = [('AND', self.work.domain, (
        # if there is a _parent_operation, return clause:
            #     ('parent', 'child_of', '_parent_operation.work')
        # else return a dummy clause, without any impact:
        #     ('id', 'not in', [])
            If(Eval('_parent_operation', False), 'parent', 'id',),
            If(Eval('_parent_operation', False), 'child_of', 'not in'),
            If(Eval('_parent_operation', False),
                Get(Eval('_parent_operation', {}), 'work', []),
                []),
        ))]
        self._reset_columns()

Line()
